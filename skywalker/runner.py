from skywalker.skywalker.spiders.program_spider import ProgramSpider
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
import os


class SpiderRunner:
    def __init__(self):
        settings_file_path = 'skywalker.skywalker.settings'
        os.environ.setdefault('SCRAPY_SETTINGS_MODULE', settings_file_path)
        self.process = CrawlerProcess(get_project_settings())
        self.spiders = [ProgramSpider]

    def run_all(self):
        for spider in self.spiders:
            self.process.crawl(spider)
            self.process.start()
