# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter
import json


class JsonWriterPipeline:
    def open_spider(self, spider):
        self.file = open(f'{spider.name}_items.json', 'w')
        self.file.write('[\n')
        self.written = 0

    def close_spider(self, spider):
        self.file.write('\n]')
        self.file.close()

    def process_item(self, item, spider):
        line = json.dumps(ItemAdapter(item).asdict())
        if self.written:
            self.file.write(',\n')
        self.file.write(line)
        self.written += 1
        return item
