import logging

import scrapy
from scrapy_selenium import SeleniumRequest
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from skywalker.skywalker.items import ProgramItem
from queue import Queue

xpath_program_links = r"//h2[@class='listing__link']/a[@id='aListingLink']"
xpath_program_detail_title = r"//h1[@id='h1_detail__header']"
xpath_program_detail_contents = r"//div[@id='div_detail__body']"
xpath_program_detail_tags = r"//div[@id='divTagsList']/a[@class='listing__tag-item listing__tag-link']"


class ProgramSpider(scrapy.Spider):
    name = "programs"
    timeout = 10

    def start_requests(self):
        urls = [
            'https://www.darpa.mil/our-research'
        ]
        self.detail_urls = Queue()
        self.num_max_drivers = self.settings.get("SELENIUM_MAX_INSTANCES", self.settings.get("CONCURRENT_REQUESTS"))
        for url in urls:
            yield SeleniumRequest(url=url, callback=self.parse)

    def parse(self, response, **kwargs):
        driver = response.request.meta['driver']
        try:
            btn_viewAll = WebDriverWait(driver, self.timeout).until(EC.presence_of_element_located((By.ID, 'aViewAll')))
            btn_viewAll.click()
            links = driver.find_elements_by_xpath(xpath_program_links)
            detail_urls = [link.get_attribute('href') for link in links]
            response.release_driver()
            for url in detail_urls:
                self.detail_urls.put_nowait(url)
            for _ in range(self.num_max_drivers):
                yield SeleniumRequest(url=self.detail_urls.get(timeout=self.timeout), callback=self.parse_detail)
        except TimeoutException as e:
            self.log(f"Timeout: loading home page {response.request.url}",logging.ERROR)

    def parse_detail(self, response, **kwargs):
        driver = response.request.meta['driver']
        try:
            title = WebDriverWait(driver, self.timeout).until(
                EC.presence_of_element_located((By.ID, 'h1_detail__header'))).text
            contents = driver.find_element_by_xpath(xpath_program_detail_contents).get_attribute("innerHTML")
            tags = [ tag.text for tag in driver.find_elements_by_xpath(xpath_program_detail_tags)]
            response.release_driver()
            yield ProgramItem(title=title, contents=contents, tags=tags, url=response.request.url)
            if not self.detail_urls.empty():
                yield SeleniumRequest(url=self.detail_urls.get(timeout=self.timeout), callback=self.parse_detail)
        except TimeoutException as e:
            self.log(f"Timeout: loading detail page {response.request.url}",logging.ERROR)
